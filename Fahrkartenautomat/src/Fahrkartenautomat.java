﻿import java.text.DecimalFormat;
import java.text.*;
import java.util.Scanner;

public class Fahrkartenautomat {

	static double fahrkartenbestellungErfassen() {
		DecimalFormat f = new DecimalFormat("#0.00");
		int t = 0;
		int a;
		double b = 0;
		String ticketName[] = { "Einzelfahrschein AB (1)", "Einzelfahrschein BC (2)", "Einzelfahrschein ABC (3)",
				"Kurzstrecke (4)", "Tageskarte AB (5)", "Tageskarte BC (6)", "Tageskarte ABC (7)",
				"Kleingruppen-Tageskarte AB (8)", "Kleingruppen-Tageskarte CB (9)",
				"Kleingruppen-Tageskarte ABC (10)" };
		double preis[] = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24, 90 };
		double fahrkarte = 0;
		double gesamt = 0;
		boolean loop = true;

		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
		System.out.println("\n");
		System.out.printf("%-35s|%-35s|\n", "Bezeichung (Auswahlnummer)", "Preis pro Ticket (EUR)");
		System.out.println("-----------------------------------|-----------------------------------|");
		for (int i = 0; i < ticketName.length; i++) {
			System.out.printf("%-35s|", ticketName[i]);
			System.out.printf("%-35s|", f.format(preis[i]) + " Euro");
			System.out.println();
		}

		System.out.println("\n");
		System.out.println("zur Zahlung (100)");
		System.out.println("\n");

		Scanner tastatur = new Scanner(System.in);

		while (loop = true) {

			while (t >= 11 || t <= 0) {
				System.out.println("Ihre Wahl: ");

				t = tastatur.nextInt();
				if (t == 100) {

					break;
				}
				if (t >= 11) {
					System.out.println(">>Bitte geben Sie eine der angegebenen Auswahlmöglichkeiten an<<");

				}
			}
			if (t == 100) {
				break;
			}

			if (t == 1) {
				fahrkarte = preis[0];
			}

			if (t == 2) {
				fahrkarte = preis[1];
			}

			if (t == 3) {
				fahrkarte = preis[2];
			}

			if (t == 4) {
				fahrkarte = preis[3];
			}

			if (t == 5) {
				fahrkarte = preis[4];
			}

			if (t == 6) {
				fahrkarte = preis[5];
			}

			if (t == 7) {
				fahrkarte = preis[6];
			}

			if (t == 8) {
				fahrkarte = preis[7];
			}

			if (t == 9) {
				fahrkarte = preis[8];
			}

			if (t == 10) {
				fahrkarte = preis[9];
			}
			do {
				System.out.print("Anzahl der Tickets: ");
				a = tastatur.nextInt();

				if (a > 11 || a < 0) {
					System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
					System.out.println("\n");
				}
			} while (a > 11 || a < 0);

			gesamt += a * fahrkarte;

			System.out.println("\n");
			System.out.println("Zwischensumme: " + f.format(gesamt) + " Euro");
			System.out.println("\n");
			t = 0;
		}

		return gesamt; // if(ticketpreis<0) {
						// ticketpreis = 1;

	}

	static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		DecimalFormat f = new DecimalFormat("#0.00");
		double gesamt;
		double münze;
		gesamt = 0.0;
		while (gesamt < zuZahlen) {
			System.out.println("Noch zu zahlen: " + (f.format(zuZahlen - gesamt) + " Euro"));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			münze = tastatur.nextDouble();
			gesamt += münze;
		}
		return gesamt - zuZahlen;
	}

	static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte();
		}

	}

	static void rückgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.00) {
			DecimalFormat f = new DecimalFormat("#0.00");
			System.out.println("Der Rückgabebetrag in Höhe von " + f.format(rückgabebetrag) + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");
		}

	}

	static void warte() {
		int millisekunde = 250;
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	static void muenzeAusgeben(double rückgabebetrag) {
		String einheit;
		if (rückgabebetrag < 1) {
			einheit = "CENT";
		} else {
			einheit = "EURO";
		}

		while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
		{
			System.out.println("2 " + einheit);
			rückgabebetrag -= 2.0;
		}
		while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
		{
			System.out.println("1 " + einheit);
			rückgabebetrag -= 1.0;
		}
		while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
		{
			System.out.println("50 " + einheit);
			rückgabebetrag -= 0.5;
		}
		while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
		{
			System.out.println("20 " + einheit);
			rückgabebetrag -= 0.2;
		}
		while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
		{
			System.out.println("10 " + einheit);
			rückgabebetrag -= 0.1;
		}
		while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
		{
			System.out.println("5 " + einheit);
			rückgabebetrag -= 0.05;
		}

	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		boolean neustart;
		while (neustart = true) {

			double zuZahlenderBetrag;
			double rückgabebetrag;

			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			// Fahrscheinausgabe
			// ----------------
			fahrkartenAusgeben();

			System.out.println("\n\n");
			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rückgeldAusgeben(rückgabebetrag);

			muenzeAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");

			System.out.println("\n");

			System.out.println("\n");

		}
		tastatur.close();
	}

}