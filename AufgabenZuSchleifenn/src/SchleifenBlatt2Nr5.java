import java.text.DecimalFormat;
import java.util.Scanner;

public class SchleifenBlatt2Nr5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double zinssatz, laufzeit, kapital;
		double startKapital;
		DecimalFormat f = new DecimalFormat("#0.00");

		Scanner scan = new Scanner(System.in);
		System.out.println("Laufzeit (in Jahren) des Sparvertrags: ");
		laufzeit = scan.nextDouble();

		System.out.println("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		kapital = scan.nextDouble();
		startKapital = kapital;

		System.out.println("Zinssatz: ");
		zinssatz = scan.nextDouble() / 100;
		scan.close();
		System.out.println("\n");

		do {
			laufzeit--;
			kapital = kapital + (kapital * zinssatz);

		} while (laufzeit > 0);

		System.out.println("Eingezahltes Kapital: " + f.format(startKapital) + " Euro");
		System.out.println("Ausgezahltes Kapital: " + f.format(kapital) + " Euro");
	}

}
