import java.util.Scanner;

public class SchleifenBlatt2Nr3 {

	public static void main(String[] args) {

		int zahl = 0;
		int quersumme = 0;

		Scanner scan = new Scanner(System.in);

		System.out.printf("Zahl eingeben: ");
		zahl = scan.nextInt();

		scan.close();

		while (zahl != 0) {

			quersumme += zahl % 10;

			zahl = zahl / 10;
		}
		System.out.print("Die Quersumme betr�gt: " + quersumme);
	}

}
