import java.util.Scanner;

public class Aufgabe1DritteMit3Zahlen {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein (Zahl 1)");
		int zahl1 = scan.nextInt();
		System.out.println("Geben Sie noch eine Zahl ein (Zahl 2)");
		int zahl2 = scan.nextInt();
		System.out.println("Geben Sie eine letzte Zahl ein (Zahl 3)");
		int zahl3 = scan.nextInt();
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println(zahl1 + " ist die gr��te Zahl");
		} else if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println(zahl2 + " ist die gr��te Zahl");
		} else {
			System.out.println(zahl3 + " ist die gr��te Zahl");
		}
		scan.close();
	}
}