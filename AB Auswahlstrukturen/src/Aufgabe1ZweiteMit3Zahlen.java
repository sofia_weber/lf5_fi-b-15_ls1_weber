import java.util.Scanner;

public class Aufgabe1ZweiteMit3Zahlen {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein (Zahl 1)");
		int zahl1 = scan.nextInt();
		System.out.println("Geben Sie noch eine Zahl ein (Zahl 2)");
		int zahl2 = scan.nextInt();
		System.out.println("Geben Sie eine letzte Zahl ein (Zahl 3)");
		int zahl3 = scan.nextInt();
		if (zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println(zahl3 + " ist mindestestens gr��er als eine der beiden anderen Zahlen.");
		}
		scan.close();
	}
}
