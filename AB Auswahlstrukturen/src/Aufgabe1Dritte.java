import java.util.Scanner;

public class Aufgabe1Dritte {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Geben Sie eine Zahl ein (Zahl 1)");

		int zahl1 = scan.nextInt();

		System.out.println("Geben Sie noch eine Zahl ein (Zahl 2)");

		int zahl2 = scan.nextInt();

		if (zahl1 < zahl2) {

			System.out.println(zahl2 + " ist gr��er als " + zahl1);

		}

		scan.close();
	}
}