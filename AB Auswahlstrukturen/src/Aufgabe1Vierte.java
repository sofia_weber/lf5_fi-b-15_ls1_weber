import java.util.Scanner;

public class Aufgabe1Vierte {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Geben Sie eine Zahl ein (Zahl 1)");

		int zahl1 = scan.nextInt();

		System.out.println("Geben Sie noch eine Zahl ein (Zahl 2)");

		int zahl2 = scan.nextInt();

		if (zahl1 >= zahl2) {

			System.out.println(zahl1 + " ist gleich oder gr��er als " + zahl2);

		}

		else {
			System.out.println(zahl1 + " ist kleiner als " + zahl2);
		}

		scan.close();
	}

}