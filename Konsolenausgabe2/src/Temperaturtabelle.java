
public class Temperaturtabelle {

	public static void main(String[] args) {

		String fa = "Fahrenheit";
		String c = "Celsius";
		String a = "-----------------------";
		char stick = '|';

		System.out.printf("\nAufgabe 3\n\n");

		System.out.printf("%-12s" + stick + "%10s\n", fa, c);

		System.out.printf("%s\n", a);

		System.out.printf("%-12d" + stick + "%10.2f\n", -20, -28.8889);

		System.out.printf("%-12d" + stick + "%10.2f\n", -10, -23.3333);

		System.out.printf("%+-12d" + stick + "%10.2f\n", -0, -17.7778);

		System.out.printf("%+-12d" + stick + "%10.2f\n", 20, -6.6667);

		System.out.printf("%+-12d" + stick + "%10.2f\n", 30, -1.1111);

	}

}
//