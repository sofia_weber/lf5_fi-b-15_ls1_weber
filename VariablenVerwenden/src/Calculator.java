import java.util.Scanner;
	//these classes import the NumberFormat
	import java.text.DecimalFormat;
	import java.text.NumberFormat;


public class Calculator {
	
 public static void main(String[] args) {
	        
	        //using double so you can calculate with decimal numbers
	        double ergebnis = 0;
	        
	        // numbers behind comma are just shown if it's useful and until the third number
	        NumberFormat nf = new DecimalFormat("##.###");
	        
	        Scanner scan = new Scanner(System.in);
	        
	        System.out.println("Enter first Number");
	        
	        double numberOne = scan.nextDouble();
	        
	        System.out.println("Enter second Number");
	        
	        double numberTwo = scan.nextDouble();
	        
	        System.out.println("Enter Operator (+ - * /)");
	        
	        String operator = scan.next();
	        
	        //switch says which of the variables changes
	        
	        switch (operator) {
	        
	        
	        //addition
	        
	        case "+" :
	            ergebnis = numberOne + numberTwo;
	                                                                         //  NumberFormat nf is used on ergebnis
	            System.out.print("Sum of the numbers is " + nf.format(ergebnis));
	            
	            // end the case with break
	            break;
	        
	        //subtraction    
	                case "-" :
	            ergebnis = numberOne - numberTwo;
	            
	            System.out.print("Difference of the number is " + nf.format(ergebnis));
	            
	            break;
	            
	        //multiplication    
	        case "*" :
	            ergebnis = numberOne * numberTwo;
	            
	            System.out.print("Product of the number is " + nf.format(ergebnis));
	            
	            break;
	            
	        //division    
	        case "/" :
	            ergebnis = numberOne / numberTwo;
	            
	            System.out.print("Quotient of the numbers is " + nf.format(ergebnis));
	            
	            break;    
	            
	        //when wrong operator is used    
	        default :
	            System.out.print("Invalid Operator");
	            
	            
	            
	            
	        
	        }
	        
	    }
	}
	    


