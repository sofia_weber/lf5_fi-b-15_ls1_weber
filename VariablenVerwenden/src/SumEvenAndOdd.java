import java.util.Scanner;

public class SumEvenAndOdd {

	public static void main(String args[]) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Type in any positive number\n");

		int n = scan.nextInt(); // eingegebene Variable

		System.out.print("even Numbers from 1 to " + n + " are: ");

		for (int i = 1; i <= n; i++) { // die schleife soll solange wiederholt werden bis so gro� wie n ist

			if (i % 2 == 0) { // i wird auf modulo gepr�ft (mit %) das hei�t ob bei dem teilen durch 2 ein
								// Restwert entsteht. wenn das nicht der fall ist wird die zahl ausgegeben

				System.out.print(i + " + ");
			}
		}

		System.out.print("\nOdd Numbers from 1 to " + n + " are: ");

		for (int i = 1; i <= n; i++) { // Zweite for schleife f�r die ungeraden zahlen

			if (i % 2 != 0) { // es wird wieder mit der modulo funktion gepr�ft, wenn es bei der durch 2
								// teilung einen rest gibt, dann wird die zahl ausgegeben

				System.out.print(i + " + ");
			}
		}

		int sumEven = 0; // um die Summen zu errechnen ben�tigen wir diese drei Variablen
		int sumOdd = 0;
		int zaehler = 1;

		while (zaehler <= n) // die schleife geht von variable z�hler bis Variable n erreicht ist das sind
								// die niedrigste und h�chste zahl

		{

			if (zaehler % 2 == 0) // alle zahlen die in der schleife durchgegangen werden und keinen rest wert in
									// der modulo rechnung
				sumEven = sumEven + zaehler; // sollen addiert werden

			else // die restlichen
				sumOdd = sumOdd + zaehler; // wenn

			zaehler = zaehler + 1; // immer mit eins aaddieren um die n�chste zahl zu pr�fen
		}

		System.out.println("\nsum of evens: " + sumEven);

		System.out.println("sum of odds: " + sumOdd);

		scan.close();
	}
}
