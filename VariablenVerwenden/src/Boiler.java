
import java.util.Scanner;
public class Boiler {
    
    public static void main(String[] args) {
        
        System.out.println("MArkuu");
        
        Scanner scan = new Scanner(System.in);
        
        //Scan the temperature String
        String temperature = scan.next();
    
        switch (temperature) { //switch method to give the program different cases of input and the reaction it should show
        
        case "200" :
            
            System.out.print("optimum temperature "); //break is the end of the case
            break;
            
        case "400" :
            
            System.out.print("excess temperature reached");
            break;
            
        case "600" :
            
            System.out.print("critical temperature, add coolant !");
            break;
            
        case "1000" :
            
            System.out.print("switch off everything and run home !");
            break;
         
        default :  // default is the case for any other type in of the String temperature
        	
            System.out.print("Wait for next message");
            break;
            
        }
        scan.close();
    }
}