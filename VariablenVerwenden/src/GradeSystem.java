
import java.util.Scanner;

public class GradeSystem {
	public static void main(String args[]) {
		System.out.println("Type in the points you achieved");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();

		scan.close();
		if (a < 0) {
			System.out.println("Invalid marks");
		} else if (a > 0 && a <= 30) {
			System.out.println("Failed");
		} else if (a > 30 && a <= 60) {
			System.out.println("3rd division");
		} else if (a > 60 && a <= 75) {
			System.out.println("2nd division");
		} else if (a > 75) {
			System.out.println("1st division");

		}
	}
}
